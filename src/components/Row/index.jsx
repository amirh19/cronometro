import React from "react";
import { Grid } from "@material-ui/core";
import "./style.css";

const Row = (props) => (
  <Grid className="cronometer-row" item xs={12}>
    {props.children}
  </Grid>
);

export default Row;
