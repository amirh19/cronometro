import React from "react";
import Watch from "@material-ui/icons/WatchLater";
import "./style.css";
const Clock = (props) => {
  return (
    <div className="cronometer" onClick={props.onClick}>
      <div className={`counter ${props.active ? "show" : ""}`}>
        {props.time}
      </div>
      <Watch
        className="watch"
        style={{
          fontSize: 40,
          color: props.active ? "#d6796e" : "#bec8d4",
          transform: "scaleX(-1)",
        }}
      />
    </div>
  );
};
export default Clock;
