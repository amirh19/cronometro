import React from "react";
import "./style.css";
const Bar = (props) => {
  return (
    <div className="bar">
      <div
        style={{
          height: props.height,
          width: props.width,
          backgroundColor: "black",
          position: "relative",
        }}
      >
        <span className="percent">{props.height + "%"}</span>
        <span className="total">{props.total}</span>
      </div>
      <span className="label">{props.label}</span>
    </div>
  );
};
export default Bar;
