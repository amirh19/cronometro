import React from "react";
import "./style.css";

const Display = (props) => {
  return <div className="display">{props.time}</div>;
};
export default Display;
