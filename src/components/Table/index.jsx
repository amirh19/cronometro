import React from "react";
import { Grid } from "@material-ui/core";
import "./style.css";

const Table = (props) => (
  <Grid className="cronometer-table" container>
    {props.children}
  </Grid>
);
export default Table;
