import React from "react";
import Bar from "../../components/Bar";
import ProjectContext from "../../ProjectContext";
import { Redirect } from "react-router-dom";
//import "./style.css";

class BarChar extends React.Component {
  static contextType = ProjectContext;
  constructor(props) {
    super(props);
    this.state = {
      WIDTH: 0,
      totalTime: 0,
    };
  }
  componentDidMount() {
    const length = this.context.projects.length;
    let BarWidth = length > 0 ? 100 / length : 0;
    const totalTime = this.context.projects.reduce(
      //take hours only, minutes and seconds are irrelevant
      (acc, project) => project.currentTime.hh + acc,
      0
    );
    this.setState({ WIDTH: BarWidth, totalTime: totalTime });
  }
  render() {
    return (
      <div
        style={{
          width: "50vw",
          height: "50vh",
          display: "flex",
          alignItems: "flex-end",
          overflow: "auto",
        }}
      >
        {this.context.projects.length > 0 ? (
          this.context.projects.map((project) => (
            <Bar
              height={
                project.currentTime.hh
                  ? this.state.totalTime / project.currentTime.hh
                  : 0
              }
              width={this.state.WIDTH}
              label={project.name}
              total={project.currentTime.hh + " horas"}
            ></Bar>
          ))
        ) : (
          <>
            <span>Aun no hay informacion</span>
            <Redirect to="/" />
          </>
        )}
      </div>
    );
  }
}
export default BarChar;
