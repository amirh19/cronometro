import React from "react";
import Clock from "../../components/Clock";
import Display from "../../components/Display";
import Row from "../../components/Row";
import Table from "../../components/Table";
import { Chip, Modal, TextField, Button } from "@material-ui/core";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import ProjectContext from "../../ProjectContext";
import AppsIcon from "@material-ui/icons/Apps";
import { ExportToCsv } from "export-to-csv";
import { Grid } from "@material-ui/core";
class Cronometer extends React.Component {
  static contextType = ProjectContext;
  constructor(props) {
    super(props);
    this.state = {
      active: -1,
      isOn: false,
      projects: [],
      currentTime: { hh: 0, mm: 0, ss: 0 },
      modal: false,
      newProjectName: "",
    };
  }
  componentDidMount() {
    let projects = [
      { name: "Proyecto 1", currentTime: { hh: 0, mm: 0, ss: 0 } },
      { name: "Proyecto 2", currentTime: { hh: 0, mm: 0, ss: 0 } },
    ];
    if (this.context.projects.length > 0) {
      projects = this.context.projects;
    }

    this.setState({ projects: projects });
    this.context.updateProject(projects);
  }
  toggleCronometer = (index) => {
    const isActive = this.state.active === index;
    const { isOn } = this.state;
    if (isActive) {
      if (isOn) {
        this.addTimeToProject(this.state.currentTime, index);
        clearInterval(this._interval);
        this.setState({
          active: -1,
          isOn: false,
          currentTime: { hh: 0, mm: 0, ss: 0 },
        });
        this.context.setCurrent(false);
      } else {
        clearInterval(this._interval);
        this.setState({ isOn: true }, () => this._start());
        this.context.setCurrent(true);
      }
    } else {
      if (!isOn) {
        this.setState({ active: index, isOn: true }, () => this._start());
        this.context.setCurrent(true);
      }
    }
  };

  addTimeToProject = (time, index) => {
    let projects = this.state.projects;
    let project = projects[index];
    const saved = project.currentTime;
    let auxSS = time.ss % 60;
    let auxMM = time.mm % 60;
    let newTime = {
      hh: saved.hh + parseInt(time.mm / 60),
      mm: saved.mm + auxMM + parseInt(time.ss / 60),
      ss: saved.ss + auxSS,
    };
    projects[index].currentTime = newTime;
    //here is a good place to update data into db.
    this.setState({ projects: projects });
    this.context.updateProject(projects);
  };
  _start = () => {
    this._interval = setInterval(this.getTime, 1000);
  };
  getTime = () => {
    let { hh, mm, ss } = this.state.currentTime;
    ss++;
    if (ss > 59) {
      mm++;
      ss = 0;
    }
    if (mm > 59) {
      hh++;
      mm = 0;
    }
    this.setState({ currentTime: { hh: hh, mm: mm, ss: ss } });
  };

  addProject = () => {
    let { projects } = this.state;
    if (
      projects.find((project) => project.name === this.state.newProjectName)
    ) {
      return this.setState({
        error: "No pueden haber dos proyectos con el mismo nombre",
      });
    }
    if (this.state.newProjectName.length === 0) {
      return this.setState({
        error: "Debe ingresar el nombre y no puede ser vacío.",
      });
    }
    projects.push({
      name: this.state.newProjectName,
      currentTime: { hh: 0, mm: 0, ss: 0 },
    });
    this.setState({
      projects: projects,
      newProjectName: "",
      modal: false,
      error: "",
    });
  };

  exportToCSV = () => {
    const options = {
      fieldSeparator: ",",
      quoteStrings: '"',
      decimalSeparator: ".",
      showLabels: true,
      showTitle: true,
      title: "Proyectos",
      useTextFile: false,
      useBom: true,
      useKeysAsHeaders: true,
      filename: "Proyectos-" + new Date().toISOString(),
      // headers: ['Column 1', 'Column 2', etc...] <-- Won't work with useKeysAsHeaders present!
    };

    const csvExporter = new ExportToCsv(options);
    csvExporter.generateCsv(
      this.state.projects.map((project) => ({
        ...project,
        currentTime: `${
          this.state.currentTime.hh >= 10
            ? this.state.currentTime.hh
            : "0" + this.state.currentTime.hh
        }:${
          this.state.currentTime.mm >= 10
            ? this.state.currentTime.mm
            : "0" + this.state.currentTime.mm
        }:${
          this.state.currentTime.ss >= 10
            ? this.state.currentTime.ss
            : "0" + this.state.currentTime.ss
        }`,
      }))
    );
  };

  render() {
    return (
      <>
        <Grid
          style={{ justifyContent: "space-around", display: "flex" }}
          item
          xs={12}
        >
          <Chip
            avatar={<AddCircleIcon style={{ fontSize: 10 }} />}
            label="add new timer"
            onClick={() => this.setState({ modal: true })}
          />
          <Chip
            avatar={<AppsIcon style={{ fontSize: 10 }} />}
            label="export to csv"
            onClick={() => this.exportToCSV()}
          />
        </Grid>
        <Table>
          {this.state.projects.map((project, index) => (
            <Row key={index}>
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <Display
                  time={`${
                    project.currentTime.hh >= 10
                      ? project.currentTime.hh
                      : "0" + project.currentTime.hh
                  }:${
                    project.currentTime.mm >= 10
                      ? project.currentTime.mm
                      : "0" + project.currentTime.mm
                  }`}
                  color="#d6796e"
                />
                <span style={{ margin: "1rem" }}>{project.name}</span>
              </div>
              <Clock
                onClick={() => {
                  this.toggleCronometer(index);
                }}
                active={this.state.active === index}
                time={
                  this.state.active === index
                    ? `${
                        this.state.currentTime.hh >= 10
                          ? this.state.currentTime.hh
                          : "0" + this.state.currentTime.hh
                      }:${
                        this.state.currentTime.mm >= 10
                          ? this.state.currentTime.mm
                          : "0" + this.state.currentTime.mm
                      }:${
                        this.state.currentTime.ss >= 10
                          ? this.state.currentTime.ss
                          : "0" + this.state.currentTime.ss
                      }`
                    : "00:00:00"
                }
              />
            </Row>
          ))}
        </Table>
        <Modal
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
          open={this.state.modal}
        >
          <div
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              width: "80vw",
              height: "50vh",
              backgroundColor: "white",
              flexDirection: "column",
            }}
          >
            <TextField
              value={this.state.newProjectName}
              label="Project Name"
              error={this.state.error ? true : false}
              onChange={(e) =>
                this.setState({ newProjectName: e.target.value })
              }
            ></TextField>
            <Button onClick={this.addProject}>Agregar</Button>
            <Button onClick={() => this.setState({ modal: false })}>
              Cancelar
            </Button>
            {this.state.error}
          </div>
        </Modal>
      </>
    );
  }
}

export default Cronometer;
