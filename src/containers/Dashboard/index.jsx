import React from "react";
import BarChar from "../../containers/BarChar";
const Dashboard = (props) => {
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <h1>Dashboard</h1>
      <p style={{ textAlign: "center" }}>
        {" "}
        Toda la información que necesita saber sobre sus proyectos
      </p>
      <em>Horas</em>
      <BarChar></BarChar>
    </div>
  );
};
export default Dashboard;
