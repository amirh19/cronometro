import React from "react";
import Cronometer from "./containers/Cronometer";
import Dashboard from "./containers/Dashboard";
import EqualizerIcon from "@material-ui/icons/Equalizer";
import { Chip, Grid } from "@material-ui/core";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import ProjectContext from "./ProjectContext";
import RotateLeftIcon from "@material-ui/icons/RotateLeft";
import "./App.css";

function App() {
  const [projects, updateProject] = React.useState([]);
  const [direction, updateDirection] = React.useState("/");
  const [current, setCurrent] = React.useState(false);
  return (
    <ProjectContext.Provider
      value={{
        projects: projects,
        updateProject: updateProject,
        setCurrent: setCurrent,
      }}
    >
      <Router>
        <Grid container>
          <Grid
            style={{
              justifyContent: "space-around",
              display: "flex",
              padding: "1rem",
            }}
            item
            xs={12}
          >
            <Link
              style={{
                pointerEvents: current ? "none" : "auto",
              }}
              to={direction === "/" ? "/dashboard" : "/"}
              onClick={() =>
                updateDirection(direction === "/" ? "/dashboard" : "/")
              }
            >
              <Chip
                style={{
                  cursor: "pointer",
                }}
                avatar={<EqualizerIcon style={{ fontSize: 10 }} />}
                label={direction === "/" ? "Dashboard" : "Home"}
              />
            </Link>
            <Chip
              avatar={<RotateLeftIcon style={{ fontSize: 10 }} />}
              label={"reset projects"}
              onClick={() =>
                updateProject(
                  projects.map((project) => ({
                    ...project,
                    currentTime: { hh: 0, mm: 0, ss: 0 },
                  }))
                )
              }
            />
          </Grid>
          <Switch>
            <Route exact path="/">
              <Cronometer />
            </Route>
            <Route exact path="/dashboard">
              <Dashboard />
            </Route>
          </Switch>
        </Grid>
      </Router>
    </ProjectContext.Provider>
  );
}

export default App;
